﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Open.ClosedPrinciple
{
    public interface IGun
    {
        public int magazine { get; set; }
        public int FireRate { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        void FireGun();
        void Reload();
        string CheckMagazine();

    }
}
