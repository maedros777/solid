﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.Substitution
{
    public interface IDeadeye
    {
        void Deadeye();
    }
}
