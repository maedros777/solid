﻿using Liskov.Substitution;

namespace Open.ClosedPrinciple
{
    public class Revolver : IGun, IDeadeye
    {
        public int magazine { get; set ; }
        public int FireRate { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        public Revolver()
        {
            magazine = 6;
        }
        public string CheckMagazine()
        {
            return magazine.ToString();
        }

        public void FireGun()
        {
            magazine -= 1;
        }

        public void Reload()
        {
            magazine += 1;
        }

        public void Deadeye()
        {
            while (magazine > 0)
            {
                FireGun();
            }
        }
    }
}
