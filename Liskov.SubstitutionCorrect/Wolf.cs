﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.SubstitutionCorrect
{
    public class Wolf : IHunt, IEat, IWalk
    {
        public string Food()
        {
            return "Meat";
        }

        public void Hunt()
        {
            Console.WriteLine("I hunt sheep");
        }

        public string LegsUsed()
        {
            return "4";
        }
    }
}
