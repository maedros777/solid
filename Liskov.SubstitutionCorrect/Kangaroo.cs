﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.SubstitutionCorrect
{
    public class Kangaroo : IWalk, IEat
    {
        public string Food()
        {
            return "Plants";
        }

        public string LegsUsed()
        {
            return "2";
        }
    }
}
