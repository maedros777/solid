﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.SubstitutionCorrect
{
    public interface IHunt
    {
        void Hunt();
    }
}
