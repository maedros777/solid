﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.SubstitutionCorrect
{
    public interface IWalk
    {
        string LegsUsed();
    }
}
