﻿using System;
using System.Collections.Generic;

namespace Liskov.SubstitutionCorrect
{
    class Program
    {
        static void Main(string[] args)
        {
            Wolf wolf = new Wolf();
            Console.WriteLine(wolf.Food());
            wolf.Hunt();
            Console.WriteLine(wolf.LegsUsed());
            Wolf wolf2 = new Wolf();
            Console.WriteLine(wolf2.Food());
            wolf2.Hunt();
            Console.WriteLine(wolf2.LegsUsed());

            Kangaroo kangaroo = new Kangaroo();
            Console.WriteLine(kangaroo.Food());
            Console.WriteLine(kangaroo.LegsUsed());

            var list = new List<IHunt>();
            list.Add(wolf);
            list.Add(wolf2);

        }
    }
}
