﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov.SubstitutionCorrect
{
    public interface IEat
    {
        string Food();
    }
}
