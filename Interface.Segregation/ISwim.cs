﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public interface ISwim
    {
        void Swim();
    }
}
