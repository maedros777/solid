﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Wolf :IWalk,ICarnivore, IHunters
    {
        public void Eat()
        {
            Console.WriteLine("I eat meat");
        }

        public void Howl()
        {
            Console.WriteLine("AUUUUuuUUUuwuwuuu");
        }

        public void Hunt()
        {
            Console.WriteLine("I hunt");
        }

        public void Walk()
        {
            Console.WriteLine("I use four legs");
        }
    }
}
