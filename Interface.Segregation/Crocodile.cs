﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Crocodile : ICarnivore, IHunters, IWalk, ISwim
    {
        public void Eat()
        {
            Console.WriteLine("I eat meat");
        }

        public void Hunt()
        {
            Console.WriteLine("I hunt");
        }

        public void Swim()
        {
            Console.WriteLine("I swim");
        }

        public void Walk()
        {
            Console.WriteLine("I use 4 legs to walk");
        }
    }
}
