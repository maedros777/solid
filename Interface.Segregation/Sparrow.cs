﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Sparrow : IInsectivore, IFlying, IHunters
    {
        public void Eat()
        {
            Console.WriteLine("I eat worms");
        }

        public void Fly()
        {
            Console.WriteLine("I fly");
        }

        public void Hunt()
        {
            Console.WriteLine("I hunt");
        }
    }
}
