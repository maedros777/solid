﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Animals
    {
        public virtual void Eat()
        {
            Console.WriteLine("I eat meat");
        }
        public virtual void Walk()
        {
            Console.WriteLine("I use four legs to walk");
        }
        public virtual void Hunt()
        {
            Console.WriteLine("I hunt");
        }
    }
}
