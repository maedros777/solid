﻿using System;

namespace Interface.Segregation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------WOlf");
            var wolf = new Wolf();
            wolf.Eat();
            wolf.Walk();
            wolf.Hunt();
            wolf.Howl();
            Console.WriteLine("---- Trutle");
            var turtle = new Turtle();
            turtle.GetInTheShell();
            turtle.Eat();
            turtle.Walk();
            Console.WriteLine("---Eagle");
            var eagle = new Eagle();
            eagle.Eat();
            eagle.Hunt();
            eagle.Fly();

            Console.WriteLine("--sparrow");
            var sparrow = new Sparrow();
            sparrow.Eat();
            sparrow.Fly();

            Console.WriteLine("----Crocodile");
            var croc = new Crocodile();
            croc.Eat();
            croc.Walk();
            croc.Hunt();
            croc.Swim();
            Console.ReadKey();
        }
    }
}
