﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Turtle : IWalk, Ihervibore
    {
        public void Eat()
        {
            Console.WriteLine("I eat grass");
        }

        public void GetInTheShell()
        {
            Console.WriteLine("I'm hidden!");
        }

        public void Walk()
        {
            Console.WriteLine("I use four legs");
        }
    }
}
