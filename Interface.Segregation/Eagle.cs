﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Segregation
{
    public class Eagle : IFlying, ICarnivore, IHunters
    {
        public void Eat()
        {
            Console.WriteLine("I eat meat");
        }

        public void Fly()
        {
            Console.WriteLine("I fly");
        }

        public void Hunt()
        {
            Console.WriteLine("I hunt");
        }
    }
}
