﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VikingSubstitution
{
    public class Ragnar : IViking
    {
        public void Die()
        {
            Console.WriteLine("Ugh I died.");
        }

        public void Drink()
        {
            Console.WriteLine("Cheers!");
        }

        public void Fight()
        {
            Console.WriteLine("Valhalla!");
        }
    }
}
