﻿using System;
using System.Collections.Generic;

namespace VikingSubstitution
{
    class Program
    {
        static void Main(string[] args)
        {
            //esto no es liskov
            Ragnar ragnar = new Ragnar();
            Ragnar ragnar2 = new Ragnar();

            //ragnar.Drink();
            //ragnar.Fight();
            //ragnar.Die();

            Ivar ivar = new Ivar();
            //ivar.Drink();
            //ivar.Fight();
            //ivar.Die();

            //esto si es liskov
            IViking ragnarReborn = new Ragnar();
            //ragnarReborn.Drink();
            //ragnarReborn.Fight();
            //ragnarReborn.Die();

            IViking ivarReborn = new Ivar();
            //ivarReborn.Drink();
            //ivarReborn.Fight();
            //ivarReborn.Die();

            IEnumerable<IViking> list = new List<IViking>();
            
            foreach (var viking in list)
            {
                viking.Drink();
            }
            Console.ReadKey();
        }
    }
}
