﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VikingSubstitution
{
    public interface IViking
    {
        void Fight();
        void Drink();
        void Die();
    }
}
