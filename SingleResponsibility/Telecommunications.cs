﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingleResponsibility
{
    public class Telecommunications
    {
        private readonly Message _message;
        private readonly User _user;

        public Telecommunications(User user, Message message)
        {
            _user = user;
            _message = message;
        }

        public void CalculateCost()
        {
            _user.MoneyInWallet = _user.MoneyInWallet - _message.Cost;
        }
    }
}
