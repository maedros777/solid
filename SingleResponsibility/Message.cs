﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingleResponsibility
{
    public class Message
    {
        public string Text { get; set; }
        public int Cost { get; set; }

        public Message(int type,string text)
        {
            switch (type)
            {
                case 0:
                    Cost = 5;
                    break;
                case 1:
                    Cost = 10;
                    break;
                case 2:
                    Cost = 20;
                    break;
                default:
                    //Handle error in a separate class thorugh a method
                    break;
            }
            Text = text;

        }
    }
}
