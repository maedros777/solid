﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingleResponsibility
{
    public class PresentationLogic
    {
        private readonly Message _message;
        private readonly User _user;
        public PresentationLogic( User user, Message message)
        {
            _message = message;
            _user = user;
        }

        public void ShowMessage()
        {
            Console.WriteLine(_message.Text);
        }

        public void ShowUserCredit()
        {
            Console.WriteLine(_user.MoneyInWallet);
        }
    }
}
