﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingleResponsibility
{
    public class User
    {
        public int MoneyInWallet { get; set; }
        public string UserName { get; set; }

        public User(int money)
        {
            MoneyInWallet = money;
        }
    }
}
