﻿using System;

namespace SingleResponsibility
{
    class Program
    {

        static void Main(string[] args)
        {
            var message = new Message(0,"Amarillo patito");
            var user = new User(100);

            var telecommunications = new Telecommunications(user,message);

            telecommunications.CalculateCost();

            var presentationLogic = new PresentationLogic(user, message);

            Console.WriteLine("---------------------");
            Console.WriteLine("Send first Message");

            presentationLogic.ShowMessage();
            presentationLogic.ShowUserCredit();

            Console.WriteLine("-------------------");
            Console.WriteLine("send new message");
            message.Text="Las rosas son rojas";
            telecommunications.CalculateCost();

            presentationLogic.ShowMessage();
            presentationLogic.ShowUserCredit();

            Console.ReadKey();
        }

    }
}
