﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Open.ClosedPrinciple
{
    public class Glock : IGun
    {
        public int magazine { get; set; }
        public int FireRate { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        public Glock()
        {
            Reload();
        }
        public string CheckMagazine()
        {
            return magazine.ToString();
        }

        public void FireGun()
        {
            magazine -= 1;
        }

        public void Reload()
        {
            magazine = 50;
        }

        public void AlternateFire(FireModes FireModes)
        {
            switch (FireModes)
            {
                case FireModes.SINGLE_SHOT:
                    FireGun();
                    break;
                case FireModes.DOUBLE_SHOT:
                    FireGun();
                    FireGun();
                    break;
                case FireModes.TRIPLE_SHOT:
                    FireGun();
                    FireGun();
                    FireGun();
                    break;
                case FireModes.FULL_AUTO:
                    while (magazine>0)
                    {
                        FireGun();
                    }
                    break;
                default:
                    Console.WriteLine("wtf?");
                    break;
            }
        }
    }
}
