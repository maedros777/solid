﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Open.ClosedPrinciple
{
    public interface IDeadeye
    {
        void Deadeye();
    }
}
