﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Open.ClosedPrinciple
{
    public class Revolver : IGun
    {
        public int magazine { get; set ; }
        public int FireRate { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }

        public Revolver()
        {
            magazine = 6;
        }
        public string CheckMagazine()
        {
            return magazine.ToString();
        }

        public void FireGun()
        {
            magazine -= 1;
        }

        public void Reload()
        {
            magazine += 1;
        }

    }
}
