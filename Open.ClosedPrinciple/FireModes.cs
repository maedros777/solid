﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Open.ClosedPrinciple
{
    public enum FireModes
    {
        SINGLE_SHOT,
        DOUBLE_SHOT,
        TRIPLE_SHOT,
        FULL_AUTO
    }
}
