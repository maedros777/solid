﻿using System;

namespace Open.ClosedPrinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            var glock = new Glock();
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            Console.WriteLine("Single Shot");
            glock.AlternateFire(FireModes.SINGLE_SHOT);
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            Console.WriteLine("Double Shot");
            glock.AlternateFire(FireModes.DOUBLE_SHOT);
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            Console.WriteLine("Triple Shot");
            glock.AlternateFire(FireModes.TRIPLE_SHOT);
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            Console.WriteLine("Reload Glock");
            glock.Reload();
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            Console.WriteLine("Full auto");
            glock.AlternateFire(FireModes.FULL_AUTO);
            Console.WriteLine($"Glock Magazine has {glock.CheckMagazine()} bullets");

            var revolver = new Revolver();
            Console.WriteLine($"Revolver Magazine has {revolver.CheckMagazine()} bullets");
            for (int i = 0; i < 3; i++)
            {
                revolver.FireGun();
            }
            Console.WriteLine($"Revolver Magazine has {revolver.CheckMagazine()} bullets");

            Console.WriteLine("Reload Revolver");
            revolver.Reload();
            Console.WriteLine($"Revolver Magazine has {revolver.CheckMagazine()} bullets");

            Console.WriteLine("Dead eye");
            revolver.DeadEye();
            Console.WriteLine($"Revolver Magazine has {revolver.CheckMagazine()} bullets");

            Console.ReadKey();



        }
    }
}
